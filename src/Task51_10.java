import java.util.HashMap;
import java.util.Map;

public class Task51_10 {
    public static void main(String[] args) throws Exception {
        // task 1:
        System.out.println("Task 1:");
        String str1_1 = "Java";
        String str1_2 = "Devcamp JAVA exercise";
        String str1_3 = "Devcamp";
        System.out.println(task1(str1_1));
        System.out.println(task1(str1_2));
        System.out.println(task1(str1_3));

        // task 2:
        System.out.println("Task 2:");
        String str2_1 = "word";
        String str2_2 = "drow";
        String str2_3 = "java";
        String str2_4 = "js";
        System.out.println(task2(str2_1, str2_2));
        System.out.println(task2(str2_3, str2_4));

        // task 3:
        System.out.println("Task 3:");
        String str3_1 = "Java";
        if (task3(str3_1.toLowerCase()) != '\0') {
            System.out.println(task3(str3_1));
        } else {
            System.out.println("NO");
        }
        String str3_2 = "Haha";
        if (task3(str3_2.toLowerCase()) != '\0') {
            System.out.println(task3(str3_2));
        } else {
            System.out.println("NO");
        }
        String str3_3 = "Devcamp";
        if (task3(str3_3.toLowerCase()) != '\0') {
            System.out.println(task3(str3_3));
        } else {
            System.out.println("NO");
        }

        // task 4:
        System.out.println("Task 4:");
        String str4_1 = "word";
        String str4_2 = "java";
        System.out.println(task4(str4_1));
        System.out.println(task4(str4_2));

        // task 5:
        System.out.println("Task 5:");
        String str5_1 = "abc";
        String str5_2 = "a1bc";
        System.out.println(!task5(str5_1));
        System.out.println(!task5(str5_2));

        // task 6:
        System.out.println("Task 6:");
        String str6_1 = "java";
        String str6_2 = "devcamp";
        System.out.println(task6(str6_1));
        System.out.println(task6(str6_2));

        // task 7:
        System.out.println("Task 7:");
        String str7 = "1234";
        System.out.println(task7(str7));

        // task 8:
        System.out.println("Task 8:");
        String str8_1 = "devcamp java";
        char c8_2 = 'a';
        char c8_3 = 'b';
        System.out.println(task8(str8_1, c8_2, c8_3));
        String str8_3 = "exercise";
        char c8_4 = 'e';
        char c8_5 = 'f';
        System.out.println(task8(str8_3, c8_4, c8_5));

        // task 9:
        System.out.println("Task 9:");
        String str9 = "I am developer";
        System.out.println(task9(str9));

        // task 10:
        System.out.println("Task 10:");
        String str10_1 = "aba";
        String str10_2 = "abc";
        System.out.println(task10(str10_1));
        System.out.println(task10(str10_2));
    }

    // task 1: Tìm và in ra các ký tự xuất hiện nhiều hơn một lần trong String cho
    // trước không phân biệt chữ hoa hay chữ thường. Nếu các ký tự trong chuỗi đều
    // là duy nhất thì xuất ra “NO”Tìm và in ra các ký tự xuất hiện nhiều hơn một
    // lần trong String cho trước không phân biệt chữ hoa hay chữ thường. Nếu các ký
    // tự trong chuỗi đều là duy nhất thì xuất ra “NO”
    public static String task1(String input) {
        Map<Character, Integer> charCountMap = new HashMap<>();
        for (int i = 0; i < input.length(); i++) {
            char c = Character.toLowerCase(input.charAt(i));
            if (Character.isLetter(c)) {
                int count = charCountMap.getOrDefault(c, 0);
                charCountMap.put(c, count + 1);
            }
        }

        StringBuilder sb = new StringBuilder();
        for (Map.Entry<Character, Integer> entry : charCountMap.entrySet()) {
            if (entry.getValue() > 1) {
                sb.append(entry.getKey());
            }
        }

        return sb.length() > 0 ? sb.toString() : "NO";
    }

    // task 2: Kiểm tra 2 chuỗi có là đảo ngược của nhau hay không. Nếu có xuất ra
    // “OK” ngược lại “KO”
    public static String task2(String s1, String s2) {
        if (s1.length() != s2.length()) {
            return "KO";
        }
        int n = s1.length();
        for (int i = 0; i < n; i++) {
            if (s1.charAt(i) != s2.charAt(n - i - 1)) {
                return "KO";
            }
        }
        return "OK";
    }

    // task 3: Tìm ký tự chỉ xuất hiện một lần trong chuỗi, nếu có nhiều hơn một thì
    // xuất ra màn hình ký tự đầu tiên. Nếu không có ký tự nào unique xuất ra “NO”
    public static char task3(String s) {
        int[] count = new int[256]; // Đếm số lần xuất hiện của các ký tự ASCII
        int n = s.length();
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            count[c]++;
        }
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            if (count[c] == 1) {
                return c;
            }
        }
        return '\0';
    }

    // task 4: Đảo ngược chuỗi
    public static String task4(String s) {
        StringBuilder sb = new StringBuilder();
        int n = s.length();
        for (int i = n - 1; i >= 0; i--) {
            sb.append(s.charAt(i));
        }
        return sb.toString();
    }

    // task 5: Kiểm tra một chuỗi có chứa chữ số hay không, nếu có in ra false ngược
    // lại true
    public static boolean task5(String s) {
        int n = s.length();
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            if (Character.isDigit(c)) {
                return true;
            }
        }
        return false;
    }

    // task 6: Đếm số lượng ký tự nguyên âm xuất hiện trong chuỗi
    public static int task6(String s) {
        int count = 0;
        String vowels = "aeiouAEIOU";
        int n = s.length();
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            if (vowels.indexOf(c) != -1) {
                count++;
            }
        }
        return count;
    }

    // task 7: Chuyển chuỗi số nguyên sang int value
    public static int task7(String s) {
        int value = 0;
        int n = s.length();
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            if (c < '0' || c > '9') {
                throw new NumberFormatException("Invalid input string: " + s);
            }
            value = value * 10 + (c - '0');
        }
        return value;
    }

    // task 8: Cho một chuỗi str, chuyển các ký tự được chỉ định sang một ký tự khác
    // cho trước
    public static String task8(String str, char oldChar, char newChar) {
        String result = str.replace(oldChar, newChar);
        return result;
    }

    // task 9: Đảo ngược các ký tự của chuỗi cách nhau bởi dấu cách
    public static String task9(String str) {
        String[] words = str.split(" ");
        StringBuilder reversedString = new StringBuilder();
        for (int i = words.length - 1; i >= 0; i--) {
            reversedString.append(words[i]).append(" ");
        }
        return reversedString.toString().trim();
    }

    // task 10: Kiểm tra chuỗi sau khi đảo ngược và chuỗi ban đầu hoàn toàn giống
    // nhau
    public static boolean task10(String str) {
        String reversedStr = new StringBuilder(str).reverse().toString();
        return str.equals(reversedStr);
    }
}
