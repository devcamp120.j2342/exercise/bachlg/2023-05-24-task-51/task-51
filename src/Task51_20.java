import java.util.ArrayList;
import java.util.Iterator;

public class Task51_20 {
    public static void main(String[] args) throws Exception {
        // Task 1:
        ArrayList<String> colors = new ArrayList<String>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Blue");
        colors.add("Yellow");
        colors.add("Purple");

        System.out.println("Task 1: " + colors);

        // Task 2:
        ArrayList<Integer> list1 = new ArrayList<>();
        list1.add(1);
        list1.add(2);
        list1.add(3);

        ArrayList<Integer> list2 = new ArrayList<>();
        list2.add(4);
        list2.add(5);
        list2.add(6);

        for (int i = 0; i < list1.size(); i++) {
            int sum = list1.get(i) + list2.get(i);
            list2.set(i, sum);
        }

        System.out.println("Task 2: " + list2);

        // Task 3:
        ArrayList<String> colors3 = new ArrayList<>();
        colors3.add("Red");
        colors3.add("Green");
        colors3.add("Blue");
        colors3.add("Yellow");
        colors3.add("Purple");

        System.out.println("Task 3: ");
        System.out.println("Number of elements in the ArrayList: " + colors3.size());

        // Task 4:
        ArrayList<String> colors4 = new ArrayList<String>();

        colors4.add("Red");
        colors4.add("Green");
        colors4.add("Blue");
        colors4.add("Yellow");
        colors4.add("Purple");

        System.out.println("Task 4: ");
        System.out.println("Phần tử thứ 4 của ArrayList: " + colors4.get(3));

        // Task 5:
        ArrayList<String> colors5 = new ArrayList<String>();
        colors5.add("Red");
        colors5.add("Green");
        colors5.add("Blue");
        colors5.add("Yellow");
        colors5.add("Orange");

        int lastIndex = colors5.size() - 1;
        String lastColor = colors5.get(lastIndex);
        System.out.println("Task 5: ");
        System.out.println("Last color in the list is: " + lastColor);

        // Task 6:
        ArrayList<String> colors6 = new ArrayList<String>();
        colors6.add("Red");
        colors6.add("Green");
        colors6.add("Blue");
        colors6.add("Yellow");
        colors6.add("Orange");

        System.out.println("Task 6: ");
        System.out.println("Original ArrayList: " + colors6);

        colors6.remove(colors6.size() - 1);

        System.out.println("ArrayList after removing the last element: " + colors6);

        // Task 7:
        ArrayList<String> colors7 = new ArrayList<String>();
        colors7.add("Red");
        colors7.add("Green");
        colors7.add("Blue");
        colors7.add("Yellow");
        colors7.add("Orange");

        System.out.println("Task 7: ");
        colors7.forEach((color) -> {
            System.out.println(color);
        });

        // Task 8:
        ArrayList<String> colors8 = new ArrayList<String>();
        colors8.add("Red");
        colors8.add("Green");
        colors8.add("Blue");
        colors8.add("Yellow");
        colors8.add("Pink");

        System.out.println("Task 8: ");
        Iterator<String> it = colors8.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }

        // Task 9:
        ArrayList<String> colors9 = new ArrayList<>();
        colors9.add("Red");
        colors9.add("Green");
        colors9.add("Blue");
        colors9.add("Yellow");
        colors9.add("Orange");

        System.out.println("Task 9: ");
        for (String color : colors9) {
            System.out.println(color);
        }

        // Task 10:
        ArrayList<String> colors10 = new ArrayList<String>();
        colors10.add("Red");
        colors10.add("Green");
        colors10.add("Blue");
        colors10.add("Yellow");
        colors10.add("Orange");
        System.out.println("Task 10: ");

        System.out.println("Initial ArrayList: " + colors10);

        colors10.add(0, "Purple");

        System.out.println("After adding color to the beginning: " + colors10);

        // Task 11:
        ArrayList<String> colors11 = new ArrayList<String>();
        colors11.add("Red");
        colors11.add("Blue");
        colors11.add("Green");
        colors11.add("Orange");
        colors11.add("Purple");
        System.out.println("Task 11: ");

        System.out.println("Original ArrayList:");
        System.out.println(colors11);

        colors11.set(2, "Yellow");
        System.out.println("Updated ArrayList:");
        System.out.println(colors11);

        // Task 12:
        ArrayList<String> names = new ArrayList<>();

        names.add("John");
        names.add("Alice");
        names.add("Bob");
        names.add("Steve");
        names.add("John");
        names.add("Steve");
        names.add("Maria");
        System.out.println("Task 12: ");

        // Tìm vị trí đầu tiên của phần tử "Alice"
        int indexAlice = names.indexOf("Alice");
        System.out.println("Vị trí đầu tiên của phần tử \"Alice\" là: " + indexAlice);

        // Tìm vị trí đầu tiên của phần tử "Mark"
        int indexMark = names.indexOf("Mark");
        System.out.println("Vị trí đầu tiên của phần tử \"Mark\" là: " + indexMark);

        // Task 13:
        ArrayList<String> names13 = new ArrayList<String>();
        names13.add("John");
        names13.add("Alice");
        names13.add("Bob");
        names13.add("Steve");
        names13.add("John");
        names13.add("Steve");
        names13.add("Maria");
        System.out.println("Task 13: ");

        int lastSteveIndex = -1;
        int lastJohnIndex = -1;

        for (int i = names13.size() - 1; i >= 0; i--) {
            String name13 = names13.get(i);
            if (name13.equals("Steve") && lastSteveIndex == -1) {
                lastSteveIndex = i;
            }
            if (name13.equals("John") && lastJohnIndex == -1) {
                lastJohnIndex = i;
            }
            if (lastSteveIndex != -1 && lastJohnIndex != -1) {
                break;
            }
        }

        System.out.println("Vị trí cuối cùng xuất hiện của Steve: " + lastSteveIndex);
        System.out.println("Vị trí cuối cùng xuất hiện của John: " + lastJohnIndex);
    }
}
