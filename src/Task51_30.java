import java.util.regex.Pattern;

public class Task51_30 {
    public static void main(String[] args) throws Exception {
        // task 1:
        System.out.println("Task 1: ");
        String str1_1 = "bananas";
        String str1_2 = "devcamp";
        System.out.println(task1(str1_1));
        System.out.println(task1(str1_2));

        // task 2:
        System.out.println("Task 2: ");
        String str2_1 = "Devcamp java";
        String str2_2 = "Abc  abc bac";
        System.out.println(task2(str2_1));
        System.out.println(task2(str2_2));

        // task 3:
        System.out.println("Task 3: ");
        String str3_1 = "abc";
        String str3_2 = "def";
        System.out.println(task3(str3_1, str3_2));

        // task 4:
        System.out.println("Task 4: ");
        String str4_1 = "Devcamp java";
        String str4_2 = "java";
        System.out.println(task4(str4_1, str4_2));

        // task 5:
        System.out.println("Task 5: ");
        String str5 = "Devcamp java";
        System.out.println(task5(str5, 3));

        // task 6:
        System.out.println("Task 6: ");
        String str6 = "Devcamp java";
        char c6 = 'a';
        System.out.println(task6(str6, c6));

        // task 7:
        System.out.println("Task 7: ");
        String str7 = "Devcamp java";
        System.out.println(task7(str7, 'a'));

        // task 8:
        System.out.println("Task 8: ");
        String str8 = "Devcamp";
        System.out.println(task8(str8));

        // task 9:
        System.out.println("Task 9: ");
        String str9 = "DevCamp";
        System.out.println(task9(str9));

        // task 10:
        System.out.println("Task 10: ");
        String str10_1 = "DevCamp";
        String str10_2 = "DevCamp123";
        System.out.println(task10(str10_1));
        System.out.println(task10(str10_2));

        // task 11:
        System.out.println("Task 11: ");
        String str11_1 = "DevCamp123";
        String str11_2 = "DEVCAMP123";
        System.out.println(task11(str11_1));
        System.out.println(task11(str11_2));

        // task 12:
        System.out.println("Task 12: ");
        String str12 = "abcdefghijklmnopqrstuvwxy";
        int n = 5;
        String[] result12 = task12(str12, n);
        if (result12 == null) {
            System.out.println("KO");
        } else {
            for (String s : result12) {
                System.out.println(s);
            }
        }

        // task 13:
        System.out.println("Task 13: ");
        String str13 = "aabaarbarccrabmq";
        String result13 = task13(str13);
        System.out.println(result13);

        // task 14:
        System.out.println("Task 14: ");
        String str14_1 = "Welcome";
        String str14_2 = "home";
        String result14 = task14(str14_1, str14_2);
        System.out.println(result14);

        // task 15:
        System.out.println("Task 15: ");
        String str15 = "DevCamp";
        System.out.println(task15(str15));

        // task 16:
        System.out.println("Task 16: ");
        String str16_1 = "DevCamp";
        String str16_2 = "DevCamp123";
        System.out.println(task16(str16_1));
        System.out.println(task16(str16_2));

        // task 17:
        System.out.println("Task 17: ");
        String str17_1 = "DevCamp123";
        String str17_2 = "DEVCAMP 123";
        System.out.println(task17(str17_1));
        System.out.println(task17(str17_2));
    }

    // task 1: Cho một chuỗi str, xoá các ký tự xuất hiện nhiều hơn một lần trong
    // chuỗi và chỉ giữ lại ký tự đầu tiên
    public static String task1(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (sb.indexOf(String.valueOf(c)) == -1) {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    // task 2: Cho một xâu kí tự, đếm số lượng các từ trong xâu kí tự đó ( các từ có
    // thể cách nhau bằng nhiều khoảng trắng )
    public static int task2(String str) {
        int count = 0;
        boolean isWord = false;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) != ' ') {
                if (!isWord) {
                    count++;
                    isWord = true;
                }
            } else {
                isWord = false;
            }
        }
        return count;
    }

    // task 3: Cho hai xâu kí tự s1, s2 nối xâu kí tự s2 vào sau xâu s1
    public static String task3(String s1, String s2) {
        return s1.concat(s2);
    }

    // task 4: Cho hai xâu kí tự s1, s2, kiểm tra xem chuỗi s1 chứa chuỗi s2 không?
    public static boolean task4(String s1, String s2) {
        return s1.contains(s2);
    }

    // task 5: Hiển thị ký tự thứ k trong string
    public static char task5(String s, int k) {
        return s.charAt(k - 1);
    }

    // task 6: đếm số lần xuất hiện của một ký tự trong một xâu
    public static int task6(String s, char c) {
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == c) {
                count++;
            }
        }
        return count;
    }

    // task 7: Tìm vị trí xuất hiện lần đầu tiên của một ký tự trong một xâu
    public static int task7(String s, char c) {
        return s.indexOf(c) + 1;
    }

    // task 8: Chuyển các ký tự in thường sang in hoa
    public static String task8(String s) {
        return s.toUpperCase();
    }

    // task 9: Chuyển các ký tự in thường sang in hoa và ngược lại
    public static String task9(String s) {
        String result = "";
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (Character.isUpperCase(c)) { // Nếu ký tự là in hoa
                result += Character.toLowerCase(c); // Chuyển sang in thường
            } else { // Ngược lại, nếu ký tự là in thường
                result += Character.toUpperCase(c); // Chuyển sang in hoa
            }
        }
        return result;
    }

    // task 10: Đếm số ký tự in hoa trong một xâu
    public static int task10(String s) {
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (Character.isUpperCase(c)) { // Nếu ký tự là in hoa
                count++; // Tăng biến đếm lên 1
            }
        }
        return count;
    }

    // task 11: Hiển thị ra màn hình các ký tự từ A tới Z
    public static String task11(String s) {
        String result = "";
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c >= 'A' && c <= 'Z') {
                result += c;
            }
        }
        return result;
    }

    // task 12: Cho một chuỗi str và số nguyên n >= 0. Chia chuỗi str ra làm các
    // phần bằng nhau với n ký tự. Nếu chuỗi không chia được thì xuất ra màn hình
    // “KO”.
    public static String[] task12(String str, int n) {
        int length = str.length();
        if (length % n != 0) {
            return null;
        }
        int numOfParts = length / n;
        String[] result = new String[numOfParts];
        int index = 0;
        for (int i = 0; i < length; i += n) {
            result[index++] = str.substring(i, i + n);
        }
        return result;
    }

    // task 13: Xoá tất cả các ký tự liền kề và giống nhau
    public static String task13(String str) {
        StringBuilder sb = new StringBuilder();
        int n = str.length();

        for (int i = 0; i < n; i++) {
            char currentChar = str.charAt(i);

            // Nếu ký tự hiện tại khác ký tự trước đó thì thêm vào chuỗi kết quả
            if (i == 0 || currentChar != str.charAt(i - 1)) {
                sb.append(currentChar);
            }
        }

        return sb.toString();
    }

    // task 14: Cho 2 string, gắn chúng lại với nhau, nếu 2 chuỗi có độ dài không
    // bằng nhau thì tiến hành cắt bỏ các ký tự đầu của string dài hơn cho đến khi
    // chúng bằng nhau thì tiến hành gắn lại
    public static String task14(String str1, String str2) {
        int len1 = str1.length();
        int len2 = str2.length();

        while (len1 > len2) {
            str1 = str1.substring(1);
            len1--;
        }
        while (len2 > len1) {
            str2 = str2.substring(1);
            len2--;
        }

        return str1.concat(str2);
    }

    // task 15: Chuyển các ký tự in thường sang in hoa và ngược lại
    public static String task15(String s) {
        String result = "";
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (Character.isUpperCase(c)) { // Nếu ký tự là in hoa
                result += Character.toLowerCase(c); // Chuyển sang in thường
            } else { // Ngược lại, nếu ký tự là in thường
                result += Character.toUpperCase(c); // Chuyển sang in hoa
            }
        }
        return result;
    }

    // task 16: Kiểm tra xem một chuỗi có xuất hiện số hay không
    public static boolean task16(String str) {
        for (char c : str.toCharArray()) {
            if (Character.isDigit(c)) {
                return true;
            }
        }
        return false;
    }

    // task 17: Kiểm tra chuỗi này có phù hợp với yêu cầu hay không
    // Yêu cầu về chuỗi là: Có độ dài không quá 20 ký tự, không được chứa ký tự
    // khoảng trắng, bắt đầu bằng một ký tự chữ viết hoa (A - Z), kết thúc bằng một
    // số (0 - 9). (Sử dụng biểu thức chính quy để ràng buộc định dạng)
    public static boolean task17(String str) {
        String regex = "^[A-Z][a-zA-Z0-9]*[0-9]$";
        Pattern pattern = Pattern.compile(regex);
        java.util.regex.Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }

}
