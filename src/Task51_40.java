import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Task51_40 {
    public static void main(String[] args) throws Exception {
        // Task 1:
        System.out.println("Task 1:");
        createAndSortArrayList();
        // Task 2:
        System.out.println("Task 2:");
        createAndFilterArrayList();
        // Task 3:
        System.out.println("Task 3:");
        createAndCheckColorArrayList();
        // Task 4:
        System.out.println("Task 4:");
        createAndSumArrayList();
        // Task 5:
        System.out.println("Task 5:");
        createAndClearArrayList();
        // Task 6:
        System.out.println("Task 6:");
        ArrayList<String> colorList = new ArrayList<>();
        colorList.add("Red");
        colorList.add("Orange");
        colorList.add("Yellow");
        colorList.add("Green");
        colorList.add("Blue");
        colorList.add("Purple");
        colorList.add("Pink");
        colorList.add("Brown");
        colorList.add("Gray");
        colorList.add("Black");
        System.out.println("ArrayList ban đầu: " + colorList);
        shuffleList(colorList);
        // Task 7:
        System.out.println("Task 7:");
        reverseList(colorList);
        // Task 8:
        System.out.println("Task 8:");
        ArrayList<String> colors = createColorList();
        System.out.println("Color list: " + colors);
        List<String> subList = getSubList(colors, 3, 7);
        System.out.println("Sub list: " + subList);
        // Task 9:
        System.out.println("Task 9:");
        swapElements(colorList);
        // Task 10:
        System.out.println("Task 10:");
        ArrayList<Integer> list1 = new ArrayList<Integer>();
        list1.add(1);
        list1.add(2);
        list1.add(3);
        System.out.println("List 1: " + list1);

        ArrayList<Integer> list2 = new ArrayList<Integer>();
        list2.add(4);
        list2.add(5);
        list2.add(6);
        System.out.println("List 2: " + list2);

        // copy list2 to list1
        Collections.copy(list1, list2);

        System.out.println("After copying List 2 to List 1");
        System.out.println("List 1: " + list1);
        System.out.println("List 2: " + list2);

    }

    // task 1: Hàm tạo mới ArrayList Integer, thêm 10 số và sắp xếp giá trị tăng dần
    public static void createAndSortArrayList() {
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(10);
        list.add(8);
        list.add(9);
        list.add(4);
        list.add(3);
        list.add(2);
        list.add(7);
        list.add(6);
        list.add(1);
        list.add(5);

        System.out.println("Original list: " + list);

        Collections.sort(list);
        System.out.println("Sorted list: " + list);
    }

    // task 2: Hàm tạo mới ArrayList Integer, thêm 10 số và tạo một ArrayList mới
    // chứa các số từ 10 đến 100
    public static void createAndFilterArrayList() {
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(10);
        list.add(200);
        list.add(40);
        list.add(15);
        list.add(500);
        list.add(50);
        list.add(150);
        list.add(35);
        list.add(60);
        list.add(310);

        System.out.println("Original list: " + list);

        ArrayList<Integer> filteredList = new ArrayList<Integer>();
        for (int i : list) {
            if (i >= 10 && i <= 100) {
                filteredList.add(i);
            }
        }

        System.out.println("Filtered list: " + filteredList);
    }

    // task 3: Hàm tạo mới ArrayList String, thêm 5 màu sắc và kiểm tra có chứa màu
    // vàng không
    public static void createAndCheckColorArrayList() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("red");
        list.add("green");
        list.add("blue");
        list.add("yellow");
        list.add("purple");

        System.out.println("Original list: " + list);

        if (list.contains("yellow")) {
            System.out.println("OK");
        } else {
            System.out.println("KO");
        }
    }

    // task 4: Hàm tạo mới ArrayList Integer, thêm 10 số và tính tổng các số
    public static void createAndSumArrayList() {
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(10);
        list.add(25);
        list.add(40);
        list.add(15);
        list.add(30);
        list.add(50);
        list.add(20);
        list.add(35);
        list.add(60);
        list.add(45);

        System.out.println("Original list: " + list);

        int sum = 0;
        for (int i : list) {
            sum += i;
        }

        System.out.println("Sum of list: " + sum);
    }

    // task 5: Hàm tạo mới ArrayList String, thêm 5 màu sắc và xóa bỏ toàn bộ giá
    // trị của ArrayList
    public static void createAndClearArrayList() {
        ArrayList<String> colors = new ArrayList<String>();
        colors.add("red");
        colors.add("green");
        colors.add("blue");
        colors.add("yellow");
        colors.add("orange");

        System.out.println("Original ArrayList: " + colors);

        colors.clear();

        colors.add("pink");
        colors.add("purple");
        colors.add("green");
        colors.add("violet");
        colors.add("black");

        System.out.println("New ArrayList: " + colors);
    }

    // task 6: Hoán đổi ngẫu nhiên vị trí các phần tử của ArrayList và ghi lại ra
    // terminal giá trị mới của ArrayList này.
    public static void shuffleList(ArrayList<String> list) {
        Collections.shuffle(list);
        System.out.println("ArrayList sau khi hoán đổi ngẫu nhiên: " + list);
    }

    // task 7: Đảo ngược vị trí các phần tử của ArrayList và ghi lại ra terminal giá
    // trị mới của ArrayList này.
    public static void reverseList(ArrayList<String> list) {
        Collections.reverse(list);
        System.out.println("ArrayList sau khi đảo ngược: " + list);
    }

    // Yêu cầu 8: Ghi ra terminal một List mới được cắt từ phần tử thứ 3 => 7 của ArrayList trên
    private static ArrayList<String> createColorList() {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Red");
        colors.add("Orange");
        colors.add("Yellow");
        colors.add("Green");
        colors.add("Blue");
        colors.add("Indigo");
        colors.add("Violet");
        colors.add("Black");
        colors.add("White");
        colors.add("Gray");
        return colors;
    }

    private static List<String> getSubList(ArrayList<String> list, int start, int end) {
        return list.subList(start, end);
    }

    // task 9: Hoán đổi vị trí của phần tử thứ 3 với phần tử thứ 7 trong ArrayList
    // và ghi lại ra terminal giá trị mới của ArrayList này.
    public static void swapElements(ArrayList<String> list) {
        Collections.swap(list, 2, 6);
        System.out.println("ArrayList sau khi hoán đổi phần tử thứ 3 và 7: " + list);
    }

}

